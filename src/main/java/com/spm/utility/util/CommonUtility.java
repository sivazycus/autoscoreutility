package com.spm.utility.util;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.spm.utility.db.ConnectionManager;
import com.spm.utility.to.KPI;
import com.spm.utility.to.Range;

public class CommonUtility {
	private static final Logger LOGGER = LoggerFactory.getLogger(CommonUtility.class);

	/**
	 * Verifying the application properties path
	 * 
	 * @param appPath
	 * @return
	 */
	public static boolean verifyApplicationPath(String appPath) {
		boolean flag = false;
		try {
			File applicationProperties = new File(appPath);
			flag = applicationProperties.exists();
		} catch (Exception ex) {
			flag = false;
		}
		if (flag) {
			LOGGER.info("Verification Successful");
		} else {
			LOGGER.error("Verification Failed");
		}
		return flag;
	}

	/**
	 * Getting all KPIS based on Autoscore type
	 * 
	 * @return
	 */
	public static List<KPI> getKPIs() {

		List<KPI> kpis = new ArrayList<>();
		String sql = "SELECT KPIID, AUTOSCORE_GROUPID FROM KPI_MASTER km WHERE AUTOSCORE_TYPE=2 ORDER by KPIID DESC";
		try (Connection con = ConnectionManager.getConnection(); PreparedStatement pstmt = con.prepareStatement(sql);ResultSet resultSet = pstmt.executeQuery();) {

			while (resultSet.next()) {
				KPI kpi = new KPI();
				kpi.setKpiId(resultSet.getLong(1));
				kpi.setAutoscoreGroupId(resultSet.getLong(2));
				kpis.add(kpi);
			}
			LOGGER.info("Successfully extracted kpiIds");
		} catch (SQLException e) {
			e.printStackTrace();
			LOGGER.error("Error while executing query to fetch all KPIs",e);
		} catch (Exception e1) {
			e1.printStackTrace();
			LOGGER.error("Error while executing query to fetch all KPIs",e1);
		}
		return kpis;
	}

	/**
	 * Getting SPM range information based on KPI
	 * 
	 * @param kpis
	 * @return
	 */
	public static Map<Long, List<Range>> getSPMRangeByKPI(List<KPI> kpis) {
		Set<Long> groupIds = kpis.stream().map(kpi -> kpi.getAutoscoreGroupId()).collect(Collectors.toSet());
		return getSPMRange(groupIds);
	}

	/**
	 * Getting SPM range information based on group id
	 * 
	 * @param kpis
	 * @return
	 */
	public static Map<Long, List<Range>> getSPMRange(Set<Long> groupIds2) {
		if (groupIds2.isEmpty()) {
			LOGGER.error("Empty groupIds to get SPM range");
			return new HashMap<Long, List<Range>>();
		}
		Map<Long, List<Range>> rangesMap = new HashMap<>();
		CharSequence delim = ",";
		String groupIds = groupIds2.stream().map(Object::toString).collect(Collectors.joining(delim));
		String sql = "SELECT RANGEID, RANGE_START, RANGE_END, RANGE_TARGET, GROUPID, POSITION FROM SPM_RANGE WHERE GROUPID IN("
				.concat(groupIds) + ")";
		try (Connection con = ConnectionManager.getConnection(); PreparedStatement pstmt = con.prepareStatement(sql);ResultSet resultSet = pstmt.executeQuery();) {
			
			while (resultSet.next()) {
				Range range = new Range();
				range.setRangeId(resultSet.getLong(1));
				range.setStart(resultSet.getLong(2));
				range.setEnd(resultSet.getLong(3));
				//range.setTarget(extractTargetValue(resultSet.getString(4)));
				Long groupId = resultSet.getLong(5);
				range.setGroupId(groupId);
				List<Range> rangeList = rangesMap.getOrDefault(groupId, new ArrayList<>());
				Long position = resultSet.getLong(6);
				range.setPosition(position);
				rangeList.add(range);
				rangesMap.put(groupId, rangeList);
			}
			LOGGER.info("Successfully extracted range info");
		} catch (SQLException e) {
			LOGGER.error("Error while fetching range detailsKPIs", e);
		} catch (Exception e1) {
			LOGGER.error("Error while fetching range detailsKPIs", e1);
		}
		return rangesMap;
	}

	/**
	 * Getting scorecard + group id information based on autoscore type and status
	 * 
	 * @param groupIds
	 * @return
	 */
	public static HashMap<Long, Long> getScorecardGroupIds() {
		HashMap<Long, Long> scorecardMap = new HashMap<>();
		String sql = "SELECT AUTOSCORE_GROUPID, SCORECARDID FROM SCORECARD_MASTER sm  WHERE AUTOSCORE_TYPE=-1 and STATUS !=1  and AUTOSCORE_GROUPID is not null";
		try (Connection con = ConnectionManager.getConnection(); PreparedStatement pstmt = con.prepareStatement(sql);ResultSet resultSet = pstmt.executeQuery();) {

			while (resultSet.next()) {
				scorecardMap.put(resultSet.getLong(1), resultSet.getLong(2));
			}
			LOGGER.info("Successfully extracted scorcard + group Ids");
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return scorecardMap;
	}
	
	
	/**
	 * Extract scorecard list
	 * 
	 * @param kpiIds
	 * @return
	 */
	public static List<Long> getScorecardByKPIIds(List<Long> kpiIds) {
		List<Long> scorecardList = new ArrayList<Long>();
		String kpiIdValue = kpiIds.stream().map(Object::toString).collect(Collectors.joining(","));
		String sql = "select SCORECARDID from SCORECARD_KPI where kpiId IN ("+ kpiIdValue+")";
		try (Connection con = ConnectionManager.getConnection(); PreparedStatement pstmt = con.prepareStatement(sql);ResultSet resultSet = pstmt.executeQuery();) {
			
			while (resultSet.next()) {
				scorecardList.add(resultSet.getLong(1));
			}
			LOGGER.info("Successfully extracted scorcard Ids");
		} catch (SQLException e) {
			LOGGER.error("Error while fetching scorecard details with custom autoscore KPIs", e);
		} catch (Exception e1) {
			LOGGER.error("Error while fetching scorecard details with custom autoscore KPIs", e1);
		}
		return scorecardList;
	}

	/**
	 * Getting all the event Ids
	 * 
	 * @param scorecardIds
	 * @return
	 */
	public static List<Long> getEventIds(List<Long> scorecardIds) {
		if (scorecardIds.isEmpty()) {
			LOGGER.error("Empty scorecardIds to get events");
			return scorecardIds;
		}
		List<Long> eventIds = new ArrayList<>();
		CharSequence delim = ",";
		String scorecardValue = scorecardIds.stream().map(Object::toString).collect(Collectors.joining(delim));
		String sql = "SELECT EVENTID FROM EVENT_MASTER em  WHERE STATUS=6 AND SCORECARDID IN (" + scorecardValue + ")";
		try (Connection con = ConnectionManager.getConnection(); PreparedStatement pstmt = con.prepareStatement(sql);ResultSet resultSet = pstmt.executeQuery();) {
			
			while (resultSet.next()) {
				eventIds.add(resultSet.getLong(1));
			}
			LOGGER.info("Successfully extracted event Ids");
		} catch (SQLException e) {
			LOGGER.error("Error while fetching event details ", e);
		} catch (Exception e1) {
			LOGGER.error("Error while fetching event details ", e1);
		}
		return eventIds;
	}

	/**
	 * Update scorecard info
	 * 
	 * @param groupIds
	 * @param autoScoreType
	 * @return
	 */
	public static int updateScorecards(String autoScoreType) {
		
		int updatedSC = 0;
		String sql = "UPDATE SCORECARD_MASTER SET AUTOSCORE_TYPE = " + autoScoreType
				+ " WHERE AUTOSCORE_TYPE = -1 AND STATUS != 1 ";
		
		if(autoScoreType.equals("0")) {
			sql += " AND AUTOSCORE_GROUPID IN (SELECT GROUPID FROM SPM_RANGE WHERE POSITION = 2) ";
		}
		try (Connection con = ConnectionManager.getConnection(); PreparedStatement pstmt = con.prepareStatement(sql);) {
			updatedSC = pstmt.executeUpdate();
			LOGGER.info("Successfully updated scorcards with auto score type : "+ autoScoreType);
		} catch (SQLException e) {
			LOGGER.error("Error while updating the scorecards with autoscore type :"+ autoScoreType, e);
		} catch (Exception e1) {
			LOGGER.error("Error while updating the scorecards with autoscore type :"+ autoScoreType, e1);
		}
		return updatedSC;
	}

	/**
	 * Update KPI information
	 * 
	 * @param groupIds
	 * @param autoScoreType
	 * @return
	 */
	public static int updateKPIs(Set<Long> groupIds, String autoScoreType) {
		if (groupIds.isEmpty()) {
			LOGGER.error("Empty groupIds to update KPI");
			return 0;
		}
		int updatedSC = 0;
		CharSequence delim = ",";
		String groupIdValue = groupIds.stream().map(Object::toString).collect(Collectors.joining(delim));
		String sql = "UPDATE KPI_MASTER set AUTOSCORE_TYPE = " + autoScoreType
				+ " WHERE AUTOSCORE_GROUPID IN(".concat(groupIdValue) + ")";
		try (Connection con = ConnectionManager.getConnection(); PreparedStatement pstmt = con.prepareStatement(sql);) {
			updatedSC = pstmt.executeUpdate();
			LOGGER.info("Successfully updated KPIs auto score type..");
		} catch (SQLException e) {
			LOGGER.error("Error while updating KPI with autoScoreType "+autoScoreType, e);
		} catch (Exception e1) {
			LOGGER.error("Error while updating KPI with autoScoreType "+autoScoreType, e1);		}
		return updatedSC;
	}

	/**
	 * Handle and Extract range target value
	 * 
	 * @param targetValue
	 * @return
	 */
	private static Double extractTargetValue(String targetValue) {
		double target = 0;
		try {
			target = Double.valueOf(targetValue.contains(".")
					? (targetValue.split("\\.")[0].trim().length() > 0 ? targetValue.split("\\.")[0] : "0")
					: (targetValue.trim().length() > 0 ? targetValue : "0"));
		} catch (Exception e) {
			// TODO: handle exception
		}
		return target;
	}

	/**
	 * Update range information
	 * 
	 * @param range
	 * @return
	 */
	public static int updateRangeInfo(Range range) {
		if (range == null) {
			LOGGER.error("Range is null and can't update range information");
			return 0;
		}
		int updatedSC = 0;
		String sql = "UPDATE SPM_RANGE SET RANGE_END=" + range.getEnd() + " WHERE RANGEID=" + range.getRangeId();
		try (Connection con = ConnectionManager.getConnection(); PreparedStatement pstmt = con.prepareStatement(sql);) {
			updatedSC = pstmt.executeUpdate();
			LOGGER.info("Successfully updated range information..");
		} catch (SQLException e) {
			LOGGER.error("Error while updating range with rangeId "+range.getRangeId(), e);
		} catch (Exception e1) {
			LOGGER.error("Error while updating range with rangeId "+range.getRangeId(), e1);
		}
		return updatedSC;

	}

	public static boolean entityWebCall(Long eventId, String spmURL) {
		CloseableHttpClient httpClient = HttpClients.createDefault();
		boolean flag = false;
		try {
			LOGGER.info("Web service call for event with eventId "+eventId);
			URIBuilder builder = new URIBuilder(spmURL);
			builder.setParameter("eventId", eventId + "").setParameter("fromStatus", "Closed").setParameter("toStatus",
					"Closed");

			HttpGet request = new HttpGet(builder.build());

			CloseableHttpResponse response = httpClient.execute(request);

			try {
				int status = response.getStatusLine().getStatusCode();
				
				System.out.println("Response status :" + response.getStatusLine().getStatusCode());

				HttpEntity entity = response.getEntity();
				String result = "";
				if (entity != null) {
					result = EntityUtils.toString(entity);
				}
				if(status == 200) {
					LOGGER.info("Response : " + result);
					flag = true;
				}else {
					flag = false;
					LOGGER.info("Response : " + result);
				}

			} finally {
				if (response != null) {
					response.close();
				}

				if (httpClient != null) {
					httpClient.close();
				}

			}
		} catch (Exception e) {
			LOGGER.error("*********** Error while making service call for event with eventId " + eventId, e);
		}
		
		return flag;
	}

	/**
	 * Prepare custom list to update the SPM range information
	 * 
	 * @param list
	 * @return
	 */
	public static Range prepareCustomListToUpdate(List<Range> list) {

		Range range0 = list.stream().filter(val -> val.getPosition() == 0).findFirst().get();
		Range range1 = list.stream().filter(val -> val.getPosition() == 1).findFirst().get();
		
		
		if (range0 != null && range1 != null && range0.getEnd() != range1.getStart()) {
			LOGGER.info("Displaying corrupted Range : "+range0);
			Range range = new Range(range0.getRangeId(), range0.getStart(), range0.getEnd(), range0.getTarget(),
					range0.getGroupId());
			range.setEnd(range1.getStart());
			return range;
		}
		return null;

	}

}
