package com.spm.utility.exception;

public class ConsulUnreachableException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ConsulUnreachableException(String message) {
		super(message);
	}
}
