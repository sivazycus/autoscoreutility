package com.spm.utility.to;

public class DBCredentials {

	private String userName = "";
	private String password = "";
	private String url = "";
	private String className = "";
	private String spmPort = "";
	private String spmIP = "";

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the className
	 */
	public String getClassName() {
		return className;
	}

	/**
	 * @param className the className to set
	 */
	public void setClassName(String className) {
		this.className = className;
	}
	

	public String getSpmPort() {
		return spmPort;
	}

	public void setSpmPort(String spmPort) {
		this.spmPort = spmPort;
	}

	public String getSpmIP() {
		return spmIP;
	}

	public void setSpmIP(String spmIP) {
		this.spmIP = spmIP;
	}

	@Override
	public String toString() {
		return "DBCredentials [userName=" + userName + ", password=" + password + ", url=" + url + ", className="
				+ className + ", spmPort=" + spmPort + ", spmIP=" + spmIP + "]";
	}

}
