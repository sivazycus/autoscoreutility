package com.spm.utility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.spm.utility.db.ConnectionManager;
import com.spm.utility.to.KPI;
import com.spm.utility.to.Range;
import com.spm.utility.util.CommonUtility;

/**
 * @author sivakumar.dhandapani
 *
 */
public class Utility {

	private static final Logger LOGGER = LoggerFactory.getLogger(Utility.class);

	private static final String CUSTOM_AUTO_SCORE_TYPE = "0";
	private static final String DEFAULT_AUTO_SCORE_TYPE = "1";
	private static List<Range> rangeToUpdate = new ArrayList<>();
	private static Set<Long> defaultGroupSet = new HashSet<>();
	private static Set<Long> customGroupSet = new HashSet<>();
	static String appPath = "/U01/iPerform/config/application.properties";

	public static void main(String[] args) {
		try {
			
			long start = System.currentTimeMillis();
			LOGGER.info("################ Auto score type utility started ###############");
			List<KPI> kpis = new ArrayList<>();
			Map<Long, List<Range>> spmRanges = new HashMap<>();
			Set<Long> scorecardGroupIds = new HashSet<>();
	
			if (CommonUtility.verifyApplicationPath(appPath)) {
				LOGGER.info("Application properties Path : " + appPath + " is correct");
			} else {
				LOGGER.error("Application properties Path : " + appPath + " is wrong");
				return;
			}
	
			ConnectionManager.setDBCredentials(appPath);
	
			String spmURL = "http://"+ConnectionManager.spmDBCredentials.getSpmIP()+":"+ConnectionManager.spmDBCredentials.getSpmPort()+"/spm/service/SPMWebServiceMaster/changeEventStatus";
	
			kpis = CommonUtility.getKPIs();
	
			spmRanges = CommonUtility.getSPMRangeByKPI(kpis);
	
			processSPMRanges(spmRanges, true);
	
			List<Long> scorcardKPIList = kpis.stream().filter(val -> customGroupSet.contains(val.getAutoscoreGroupId())).map(val -> val.getKpiId()).collect(Collectors.toList());
			
			LOGGER.info("Default group Ids to update KPI : " + defaultGroupSet);
			int updatedDefaultKPI = CommonUtility.updateKPIs(defaultGroupSet, DEFAULT_AUTO_SCORE_TYPE);
			LOGGER.info("Successfully updated Total default KPI count : " + updatedDefaultKPI);
	
			LOGGER.info("Custom group Ids to update KPI : " + customGroupSet);
			int updatedCustomKPI = CommonUtility.updateKPIs(customGroupSet, CUSTOM_AUTO_SCORE_TYPE);
			LOGGER.info("Successfully updated Total custom KPI count : " + updatedCustomKPI);
	
			int totalUpdatedRange = 0;
			for (Range range : rangeToUpdate) {
				int updateCount = CommonUtility.updateRangeInfo(range);
				totalUpdatedRange += updateCount;
				LOGGER.info("Successfully updated Range info based on KPI : " + range);
			}
			LOGGER.info("Successfully updated Total Range count based on KPI : " + totalUpdatedRange);
	
			defaultGroupSet.clear();
			customGroupSet.clear();
			rangeToUpdate.clear();
			spmRanges.clear();
	
			List<Long> scorecardList = CommonUtility.getScorecardByKPIIds(scorcardKPIList);
			LOGGER.info("Corrupted data's scorecardList : " + scorecardList);
			
			List<Long> getEventIds = CommonUtility.getEventIds(scorecardList);
			LOGGER.info("Event ids based on the scorecardList : " + getEventIds);
			
			int count = 0;
			List<Long> failedEventIds = new ArrayList<>();
			for (Long eventId : getEventIds) {
				boolean flag = CommonUtility.entityWebCall(eventId, spmURL);
				if(flag) {
					++count;
				}else {
					failedEventIds.add(eventId);
				}
			}
			LOGGER.info("Successfully updated Total Event count : " + getEventIds.size()+" Actual events recalculated scores count "+count);
			LOGGER.info("Failed Event Ids :"+failedEventIds);
			long end = System.currentTimeMillis() - start;
			LOGGER.info("############## Auto score type utility ended (in secs) : " + end/1000 + " ################");
			
		}catch(Exception e) {
			LOGGER.error("Error while running the utility ", e);
		}
	}

	private static void processSPMRanges(Map<Long, List<Range>> spmRanges, boolean calculateRange) {
		for (Entry<Long, List<Range>> entry : spmRanges.entrySet()) {
			if (entry.getValue().size() > 2) {
				List<Long> customList = entry.getValue().stream().map(val -> val.getGroupId())
						.collect(Collectors.toList());
				customGroupSet.addAll(customList);
				if(calculateRange) {
					Range range = CommonUtility.prepareCustomListToUpdate(entry.getValue());
					if (range != null) {
						
						rangeToUpdate.add(range);
					}
				}
			} else {
				List<Long> defaultList = entry.getValue().stream().map(val -> val.getGroupId())
						.collect(Collectors.toList());
				defaultGroupSet.addAll(defaultList);
			}
		}
	}

}
