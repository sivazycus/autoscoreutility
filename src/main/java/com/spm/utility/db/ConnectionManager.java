package com.spm.utility.db;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.spm.utility.to.DBCredentials;

public class ConnectionManager {

	private static final Logger LOGGER = LoggerFactory.getLogger(ConnectionManager.class);

	public static DBCredentials spmDBCredentials = new DBCredentials();

	/**
	 * Getting connection
	 * 
	 * @return
	 * @throws Exception
	 */
	public static Connection getConnection() throws Exception {
		Connection connection = null;
		try {
			String driver = spmDBCredentials.getClassName();
			String url = spmDBCredentials.getUrl();
			String user = spmDBCredentials.getUserName();
			String password = spmDBCredentials.getPassword();
			Class.forName(driver);
			connection = DriverManager.getConnection(url, user, password);
		} catch (SQLException e) {
			LOGGER.error("[inside getConnection()] : SQLException occured... while connecting to DB. DBCredentials : "
					+ spmDBCredentials, e);
			throw e;
		}
		return connection;
	}

	/**
	 * Setting DB credentials
	 * 
	 * @param filePath
	 */
	public static void setDBCredentials(String filePath) {

		String connectionURL = "";
		String dbDriverClass = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
		String userName = "";
		String password = "";
		String spmPort = "";
		try {
			InputStream appInputFile = new FileInputStream(filePath);
			Properties appProperties = new Properties();
			appProperties.load(appInputFile);

			connectionURL = appProperties.getProperty("spring.datasource.url");
			userName = appProperties.getProperty("spring.datasource.username");
			password = appProperties.getProperty("spring.datasource.password");
			spmPort = appProperties.getProperty("server.port");;

			spmDBCredentials.setUserName(userName);
			spmDBCredentials.setPassword(password);
			spmDBCredentials.setUrl(connectionURL);
			spmDBCredentials.setClassName(dbDriverClass);
			spmDBCredentials.setSpmIP(InetAddress.getLocalHost().getHostAddress());
			spmDBCredentials.setSpmPort(spmPort);

		} catch (IOException e) {
			LOGGER.error("IOException while getting DB Credentials. ", e);
		} catch (Exception e) {
			LOGGER.error("Exception while getting DB Credentials. ", e);
		}
	}
}
