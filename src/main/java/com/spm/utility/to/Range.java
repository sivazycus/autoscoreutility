/**
 * 
 */
package com.spm.utility.to;

public class Range {

	public Range() {

	}

	public Range(long rangeId, double start, double end, double target, long groupId) {
		super();
		this.rangeId = rangeId;
		this.start = start;
		this.end = end;
		this.target = target;
		this.groupId = groupId;
	}

	private long rangeId;
	private double start;
	private double end;
	private double target;
	private long groupId = 0;
	private long position;

	/**
	 * @return the rangeId
	 */
	public long getRangeId() {
		return rangeId;
	}

	/**
	 * @param rangeId the rangeId to set
	 */
	public void setRangeId(long rangeId) {
		this.rangeId = rangeId;
	}

	/**
	 * @return the start
	 */
	public double getStart() {
		return start;
	}

	/**
	 * @param start the start to set
	 */
	public void setStart(double start) {
		this.start = start;
	}

	/**
	 * @return the end
	 */
	public double getEnd() {
		return end;
	}

	/**
	 * @param end the end to set
	 */
	public void setEnd(double end) {
		this.end = end;
	}

	/**
	 * @return the groupId
	 */
	public long getGroupId() {
		return groupId;
	}

	/**
	 * @param groupId the groupId to set
	 */
	public void setGroupId(long groupId) {
		this.groupId = groupId;
	}

	/**
	 * @return the target
	 */
	public double getTarget() {
		return target;
	}

	/**
	 * @param target the target to set
	 */
	public void setTarget(double target) {
		this.target = target;
	}

	/**
	 * @return the position
	 */
	public long getPosition() {
		return position;
	}
	
	/**
	 * @param position the position to set
	 */
	public void setPosition(long position) {
		this.position = position;
	}

	@Override
	public String toString() {
		return "Range [rangeId=" + rangeId + ", start=" + start + ", end=" + end + ", target=" + target + ", groupId="
				+ groupId + ", position=" + position + "]";
	}


}
