package com.spm.utility.to;

public class Scorecard {

	private long scorecardId;
	private long autoscoreGroupId;

	/**
	 * @return the scorecardId
	 */
	public long getScorecardId() {
		return scorecardId;
	}

	/**
	 * @param scorecardId the scorecardId to set
	 */
	public void setScorecardId(long scorecardId) {
		this.scorecardId = scorecardId;
	}

	/**
	 * @return the autoscoreGroupId
	 */
	public long getAutoscoreGroupId() {
		return autoscoreGroupId;
	}

	/**
	 * @param autoscoreGroupId the autoscoreGroupId to set
	 */
	public void setAutoscoreGroupId(long autoscoreGroupId) {
		this.autoscoreGroupId = autoscoreGroupId;
	}

	public Scorecard() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "Scorecard [scorecardId=" + scorecardId + ", autoscoreGroupId=" + autoscoreGroupId + "]";
	}

}
