package com.spm.utility;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.spm.utility.db.ConnectionManager;
import com.spm.utility.to.Range;
import com.spm.utility.util.CommonUtility;

public class UtilityRollbackMain {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Utility.class);
	
	private static final String AUTO_SCORE_TYPE = "2";
	private static List<Range> rangeToUpdate = new ArrayList<>();
	private static Set<Long> defaultGroupSet = new HashSet<>();
	private static Set<Long> customGroupSet = new HashSet<>();
	static String appPath = "/U01/iPerform/config/application.properties";
	
	public static void main(String[] args) {
		
		try {
			
			LOGGER.info("################ ROLLBACK STARTS ###############");
			if (CommonUtility.verifyApplicationPath(appPath)) {
				LOGGER.info("Application properties Path : " + appPath + " is correct");
			} else {
				LOGGER.error("Application properties Path : " + appPath + " is wrong");
				return;
			}

			ConnectionManager.setDBCredentials(appPath);
			
			LOGGER.info("----- Extracting the data form the log file ------");
			String logPath = System.getProperty("user.dir")+"/upgradeLog.log";
			
			readFile(logPath);
			
			LOGGER.info("Default group Ids to update KPI : " + defaultGroupSet);
			int updatedDefaultKPI = CommonUtility.updateKPIs(defaultGroupSet, AUTO_SCORE_TYPE);
			LOGGER.info("Successfully updated Total default KPI count : " + updatedDefaultKPI);

			LOGGER.info("Custom group Ids to update KPI : " + customGroupSet);
			int updatedCustomKPI = CommonUtility.updateKPIs(customGroupSet, AUTO_SCORE_TYPE);
			LOGGER.info("Successfully updated Total custom KPI count : " + updatedCustomKPI);
			
			int totalUpdatedRange = 0;
			for (Range range : rangeToUpdate) {
				int updateCount = CommonUtility.updateRangeInfo(range);
				totalUpdatedRange += updateCount;
				LOGGER.info("Successfully updated Range info based on KPI : " + range);
			}
			LOGGER.info("Successfully updated Total Range count based on KPI : " + totalUpdatedRange);
			
			
			LOGGER.info("################ ROLLBACK COMPLETE ###############");
		}catch(Exception e) {
			LOGGER.error("Error while CSCR rollback ", e);
		}
		
		
	}
	
	static void readFile(String logPath) throws Exception {
		
		try(FileInputStream fstream = new FileInputStream(logPath);BufferedReader br = new BufferedReader(new InputStreamReader(fstream));){
			   String line;
			   while ((line = br.readLine()) != null )   {
				   if(line.contains("Custom group Ids to update KPI")) {
					   customGroupSet = process(line);
				   }
				   
				   if(line.contains("Default group Ids to update KPI")) {
					   defaultGroupSet = process(line);
				   }
				   
				   if(line.contains("Displaying corruptted Range")) {
					   Range range = processRangeDetails(line);
					   LOGGER.info("Corrupted Range "+range);
					   rangeToUpdate.add(range);
				   }
				   
			     
			   }
		} catch (Exception e) {
			LOGGER.error("Error while reading the log file " + e.getMessage(), e);
			throw e;
		}
	}
	
	static Set<Long> process(String line) {
		
	   int index = line.lastIndexOf("[");
	   line = line.substring(index);
	   line = line.replace("[", "").replace("]", "").trim();
	   List<String> strList = !line.equals("") ? Arrays.asList(line.split(",")): null;
	   
	   if(strList != null && !strList.isEmpty()) {
		   return strList.stream().map(value -> Long.parseLong(value.trim())).collect(Collectors.toSet());
	   }
	   return new HashSet<Long>();
	}
	
	static Range processRangeDetails(String line) {
		Range range = new Range();
		int index = line.lastIndexOf("[");
		line = line.substring(index);
		line = line.replace("[", "").replace("]", "").trim();
		String[] strArr = line.split(",");
		for(String s : strArr) {
			s = s.trim();
			int i = s.indexOf("=")+1;
			String value = s.substring(i).trim();
			if(s.contains("rangeId")) {
				Long rangeId = Long.parseLong(value);
				range.setRangeId(rangeId);
			}else if(s.contains("end")) {
				Double end = Double.parseDouble(value);
				range.setEnd(end);
			}
		}
		return range;
	}

}
