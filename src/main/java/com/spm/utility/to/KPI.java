package com.spm.utility.to;

public class KPI {
	private long kpiId;
	private long autoscoreGroupId;

	public KPI(long kpiId, long autoscoreGroupId) {
		super();
		this.kpiId = kpiId;
		this.autoscoreGroupId = autoscoreGroupId;
	}

	public KPI() {

	}

	/**
	 * @return the autoscoreGroupId
	 */
	public long getAutoscoreGroupId() {
		return autoscoreGroupId;
	}

	/**
	 * @param autoscoreGroupId the autoscoreGroupId to set
	 */
	public void setAutoscoreGroupId(long autoscoreGroupId) {
		this.autoscoreGroupId = autoscoreGroupId;
	}

	/**
	 * @return the kpiId
	 */
	public long getKpiId() {
		return kpiId;
	}

	/**
	 * @param kpiId the kpiId to set
	 */
	public void setKpiId(long kpiId) {
		this.kpiId = kpiId;
	}

	@Override
	public String toString() {
		return "KPI [kpiId=" + kpiId + ", autoscoreGroupId=" + autoscoreGroupId + "]";
	}
}
