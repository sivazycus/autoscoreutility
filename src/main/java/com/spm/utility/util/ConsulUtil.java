package com.spm.utility.util;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.spm.utility.exception.ConsulUnreachableException;

import java.io.IOException;

public class ConsulUtil {

	private static final Logger LOGGER = LoggerFactory.getLogger(ConsulUtil.class);

	/**
	 * Getting SPM Url from consul
	 * 
	 * @param url
	 * @return
	 * @throws IOException
	 * @throws JSONException
	 * @throws ConsulUnreachableException
	 */
	public static String getSPMUrl(String url) throws IOException, JSONException, ConsulUnreachableException {
		String spmURL = "";
		HttpGet get = new HttpGet(url);
		String emptyData = "[]";
		String consulResult = "";

		try (CloseableHttpClient httpClient = HttpClients.createDefault();
				CloseableHttpResponse response = httpClient.execute(get)) {
			consulResult = EntityUtils.toString(response.getEntity());
			if (null == consulResult || consulResult.isEmpty() || "".equalsIgnoreCase(consulResult)
					|| consulResult.equals(emptyData)) {
				return null;
			}
			int status = response.getStatusLine().getStatusCode();
			if (status == 200) {
				spmURL = parseResult(consulResult);
			} else {
				LOGGER.error(String.format("Response Status :%s", status));
				throw new ConsulUnreachableException(String.format("Response Status :%s", status));
			}
		} catch (IOException | JSONException e) {
			LOGGER.debug("Exception :", e);
		}

		return spmURL;
	}

	/**
	 * Parsing the result to fetch corresponding values for IP and port
	 * 
	 * @param consulResult
	 * @return
	 * @throws JSONException
	 */
	private static String parseResult(String consulResult) throws JSONException {
		JSONArray consulResultArray = new JSONArray(consulResult);
		String spmIP = "";
		String spmPort = "";
		String tags = "";
		for (int i = 0; i < consulResultArray.length(); i++) {
			JSONObject serviceObject = consulResultArray.getJSONObject(i).getJSONObject("Service");
			tags = serviceObject.getString("Tags");
			spmIP = serviceObject.getString("Address");
			spmPort = serviceObject.getString("Port");
			if (spmIP != null && tags != null && spmPort != null && !tags.contains("common") && isvalidIP(spmIP)) {
				return String.format(
						"http://%s:%s/spm/service/SPMWebServiceMaster/changeEventStatus",
						spmIP, spmPort);
			}
		}
		return null;
	}

	/**
	 * Validating IP
	 * 
	 * @param spmIP
	 * @return
	 */
	private static boolean isvalidIP(String spmIP) {
		String[] splittedIP = spmIP.split("\\.");
		for (String ip : splittedIP) {
			try {
				Integer.valueOf(ip);
			} catch (Exception e) {
				return false;
			}
		}
		return true;
	}

}
